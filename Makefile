EMACS=emacs
FILES=semantic-bookmarks.el

.PHONY: help package elpa clean make-test compile-test test lint

help:
	@printf "\
Main targets\n\
compile    -- compile .el files\n\
elpa 	   -- create a package with the elpa format \n\
package    -- create a tar.gz file with the .el files \n\
test       -- run tests in batch mode\n\
clean      -- delete generated files\n\
lint       -- run package-lint in batch mode\n\
help       -- print this message\n"

package: *.el
	@ver=`grep -o "Version: .*" semantic-bookmarks.el | cut -c 10-`; \
	tar czvf semantic-bookmarks-$$ver.tar.gz --mode 644 $$(find . -name \*.el)

elpa: *.el
	@version=`grep -o "Version: .*" semantic-bookmarks.el | cut -c 10-`; \
	dir=semantic-bookmarks-$$version; \
	mkdir -p "$$dir"; \
	cp $$(find . -name \*.el) semantic-bookmarks-$$version; \
	echo "(define-package \"semantic-bookmarks\" \"$$version\" \
	\"Modular in-buffer completion framework\")" \
	> "$$dir"/semantic-bookmarks-pkg.el; \
	tar cvf semantic-bookmarks-$$version.tar --mode 644 "$$dir"

clean:
	@rm -rf semantic-bookmarks-*/ semantic-bookmarks-*.tar semantic-bookmarks-*.tar.bz2 *.elc ert.el .elpa/

make-test:
	${EMACS}  --batch -l test/make-install.el -l test/make-test.el 

test: make-test clean

compile:
	${EMACS} --batch  -l test/make-install.el -L . -f batch-byte-compile semantic-bookmarks.el semantic-bookmarks*-.el

compile-test: compile clean

lint:
	${EMACS} --batch -l test/make-install.el -f package-lint-batch-and-exit ${FILES}
